import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxMotionComponent } from './box-motion.component';

describe('BoxMotionComponent', () => {
  let component: BoxMotionComponent;
  let fixture: ComponentFixture<BoxMotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxMotionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BoxMotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
