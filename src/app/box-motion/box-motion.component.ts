import { Component } from '@angular/core';
import { LogService } from '../services/log.service';
import { RawdataService } from '../services/rawdata.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-box-motion',
  templateUrl: './box-motion.component.html',
  styleUrls: ['./box-motion.component.css']
})
export class BoxMotionComponent {
  constructor(private logService: LogService, private rawdataService: RawdataService,private http:HttpClient) { }
  selectedParts: number = 1;
  duration: number = 0;
  countdown: number = 0;
  interval: any;
  poseNumber: number = 0;
  description: string = '';

  ngOnInit(): void {
    const config = localStorage.getItem('config');
    if (config) {
      // "deney_no" input'unu seç
      const deneyNoInput = document.querySelector('#config') as HTMLInputElement;
      const customInput = document.querySelector('#custom_config') as HTMLInputElement;

      // "deney_no" input'unun değerini "config" değeri ile doldur
      deneyNoInput.value = config;
      customInput.value = config;
    }
  }
  onPartsChange(event: any) {
    this.selectedParts = event.target.value;
  }

  onDurationChange(event: any) {
    this.duration = event.target.value;
  }
  calculateParts() {
    return this.selectedParts;
  }
  // }
  // calculateParts() {
  //   return Math.round((this.duration / 2) * 10) / 10;
  // }
  save() {
    const repetitions = Number((<HTMLInputElement>document.getElementById("inputRepetitions")).value);
    const parts = Number((<HTMLInputElement>document.getElementById("inputParts")).value);
    const duration_part = this.duration / parts; // toplam süre 2'ye bölünerek her bir part için süre hesaplanacak
    const delay = 1// Number((<HTMLInputElement>document.getElementById("inputDelay")).value);
    const username = (<HTMLInputElement>document.getElementById("inputName")).value;
    const expert = (<HTMLInputElement>document.getElementById("expert_name")).value;
    const company = (<HTMLInputElement>document.getElementById("company_name")).value;
    const total_duration = Number((<HTMLInputElement>document.getElementById("inputDuration")).value);
    const config = localStorage.getItem("config");
    const posenumber = Number((<HTMLInputElement>document.getElementById("inputPoseNumber")).value);
    const description = (<HTMLInputElement>document.getElementById("inputDescription")).value;

    if (company == "" || expert == "" || username == "" || repetitions == 0 || total_duration == 0 || posenumber == 0) {
      alert("Please fill all the required fields");
      return;
    }
    let repetitionCount = 0;
    let partCount = 1; // başlangıçta ilk parttan başlanacak
    let countdown = duration_part; // geri sayım her partta duration kadar olacak
    let totalDuration = this.duration;

    const popup: any = window.open("", "Countdown", "width=800,height=600");

    popup.document.body.innerHTML = `
    <div style="font-size: 80px; font-weight: bold; text-align: center;">Wait...</div>
  `;
  const sendRawDataRequest = () => {
    const rawdataToggleValue = localStorage.getItem('rawDataToggle');
    if (rawdataToggleValue === 'true' || rawdataToggleValue === 'false') {
      const isToggleChecked = rawdataToggleValue === 'true';
      try {
        const total_time = total_duration;
        const rawdata = {
          repeat: repetitionCount +1 // Add the value of "repetitions" to the rawdata object
        };
        this.rawdataService.toggleSwitchRawData(isToggleChecked, total_time, rawdata);
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log("Raw Data kapalı");
      console.log("Geçersiz rawDataToggle değeri!");
    }
  };
    setTimeout(() => {
      let startTimestamp = 0.0; // Başlangıç zamanı sıfırla
      let endTimestamp = 0.0;
      let logs: any = [];
      let wait = false; // Wait ekranının gösterilip gösterilmeyeceği kontrol ediliyor
      let waitCounter = 0; // Wait ekranının kaç saniye gösterileceği sayılıyor
      sendRawDataRequest();
      if (partCount === 1) {
        startTimestamp = Date.now() / 1000; // Part 1'in başlangıç zamanını al
        console.log("start: " + startTimestamp);
      }
      const countdownInterval = setInterval(() => {
        countdown -= 0.1;
        const formattedCountdown = this.formatTime(countdown);
        const formattedTotalTime = this.formatTime(totalDuration);
        if (wait) { // Wait ekranı gösteriliyorsa
          waitCounter += 0.1; // Wait ekranı kaç saniye gösterildiği hesaplanıyor
          if (waitCounter >= 5) { // 5 saniye bekledikten sonra
            console.log(waitCounter);
            wait = false; // Wait ekranı kapatılıyor
            waitCounter = 0;
            sendRawDataRequest();
            partCount = 1; // Part 1 geri sayımı başlatılıyor
            countdown = duration_part;
            if (partCount === 1) {
              startTimestamp = Date.now() / 1000; // Yeni tekrarın başlangıç zamanını al
              console.log("start: " + startTimestamp);
            } else {
              startTimestamp = endTimestamp; // Yeni tekrarın başlangıç zamanını bitiş zamanı olarak ayarla
              endTimestamp = Date.now() / 1000; // Bitiş zamanını güncelle
              console.log("start: " + startTimestamp);
            }            
          } else {
            popup.document.body.innerHTML = `
          <div style="font-size: 80px; font-weight: bold; text-align: center;">Wait...</div>
        `;
            return;
          }
        }

            popup.document.body.innerHTML = `
          <div style="font-size: 80px; font-weight: bold; text-align: center;">${formattedCountdown}</div>
          <div style="font-size: 30px; font-weight: bold; text-align: center;">Part ${partCount} / ${parts}</div>
          <div style="font-size: 30px; font-weight: bold; text-align: center;">Repetition ${repetitionCount + 1} / ${repetitions}</div>
          <div style="text-align: center;">
            <label for="duration" style="font-size: 20px; font-weight: bold;">Total Duration:</label>
            <label id="duration" style="font-size: 20px;">${formattedTotalTime}</label>
          </div>
        `;
        if (countdown <= 0) {
          if (partCount === 1) {
            // startTimestamp = Date.now() / 1000; // Part 1'in başlangıç zamanını al
            endTimestamp = Date.now() / 1000
            console.log("endtime" + endTimestamp);
          } else {
            startTimestamp = endTimestamp; // Bir önceki partın başlangıç zamanını kaydet
            endTimestamp = Date.now() / 1000; // Yeni partın başlangıç zamanını kaydet
            console.log(endTimestamp);
            const duration = endTimestamp - startTimestamp; 
            if (partCount >= parts) {
              wait = true; // Wait ekranı gösterilecek
             // this.rawdataService.stopRawData();
            } else if (partCount === 1 && repetitionCount > 1) {
              wait = true; // Wait ekranı gösterilecek
            }
          }
          const options = { timeZone: 'Europe/Istanbul' };
          const dateTime = new Date().toLocaleString('tr-TR', options);
          const inputs = {
            company: company,
            expert: expert,
            username: username,
            current_part: partCount,
            total_number_part: parts, // toplam parti sayısı hesaplanıyor
            current_repeat: repetitionCount + 1,
            total_number_repeat: repetitions,
            duration_part: duration_part,
            total_duration: total_duration,
            startTimestamp_current_part: startTimestamp,
            endTimestamp_current_part: endTimestamp,
            date: dateTime,
            //date: new Date().toISOString().substr(0, 10), // bugünün tarihi (yyyy-mm-dd) ekleniyor,
            config: config,
            posenumber: posenumber,
            description: description
            //delay: delay,
          };

          logs.push(inputs);

          totalDuration -= duration_part; // partın süresi totalDuration'dan çıkarılıyor
          partCount++;

          if (partCount > parts) {
            repetitionCount++;
            this.rawdataService.stopRawData();
            if (repetitionCount >= repetitions) {
              this.postLogs(logs);
              clearInterval(countdownInterval);
              popup.alert("Finish.");
              popup.close();
              return;
            }

            partCount = 1;
            totalDuration = this.duration; // yeni bir tekrar başlayacağı için totalDuration tekrar programın başlangıç değeri olmalı
            if (repetitions > 1) {
              wait = true; // Wait ekranı gösterilecek
            }
          }

          countdown = duration_part;
        }
        //Belki sonradan eklenebilir.
        //localStorage.removeItem('config');
      }, delay * 100);
    }, 5000);
  }

  postLogs(logs: any[]) {
    this.logService.postLog(logs)
      .subscribe((data) => {
        console.log('Logs posted successfully', data);
      }, (error) => {
        console.log('Error while posting logs: ', error);
      });
  }
  
  getStarted() {
    const repetitions = 3; // örnek olarak 3 tekrar
    const duration = 10; // saniye cinsinden
    const delay = 1; // saniye cinsinden
    const username = (<HTMLInputElement>document.getElementById("username")).value;
    const expert = (<HTMLInputElement>document.getElementById("expert")).value;
    const company = (<HTMLInputElement>document.getElementById("company")).value;
    const total_duration = (<HTMLInputElement>document.getElementById("inputDuration")).value;
    const posenumber = Number((<HTMLInputElement>document.getElementById("inputPoseNumberGetStarted")).value);
    const description = (<HTMLInputElement>document.getElementById("inputDescriptionGetStarted")).value;
    const duration_part = duration;
    let repetitionCount = 0;
    let partCount = 1;
    let countdown = duration_part;
    let totalDuration = repetitions * duration_part;
    let startTimestamp = 0.0;
    let endTimestamp = 0.0;
    let logs: any = [];
    const config = localStorage.getItem("config");


    if (company == "" || expert == "" || username == "" || posenumber == 0) {
      alert("Please fill all the required fields");
      return;
    }
    const popup: any = window.open("", "Countdown", "width=800,height=600");

    popup.document.body.innerHTML = `
    <div style="font-size: 80px; font-weight: bold; text-align: center;">Wait...</div>
  `;
  const sendRawDataRequest = () => {
    const rawdataToggleValue = localStorage.getItem('rawDataToggle');
    if (rawdataToggleValue === 'true' || rawdataToggleValue === 'false') {
      const isToggleChecked = rawdataToggleValue === 'true';
      try {
        const total_time = duration;
        const rawdata = {
          repeat: repetitionCount +1 // Add the value of "repetitions" to the rawdata object
        };
        this.rawdataService.toggleSwitchRawData(isToggleChecked, total_time, rawdata);
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log("Raw Data kapalı");
      console.log("Geçersiz rawDataToggle değeri!");
    }
  };
  
    setTimeout(() => {
      let wait = false; // Wait ekranının gösterilip gösterilmeyeceği kontrol ediliyor
      let waitCounter = 0;
      sendRawDataRequest();
       if (partCount === 1) {
        startTimestamp = Date.now() / 1000; // Part 1'in başlangıç zamanını al
        console.log("start: " + startTimestamp);
      }
      const countdownInterval = setInterval(() => {
        countdown -= 0.1;
        const formattedCountdown = this.formatTime(countdown);
        const formattedTotalTime = this.formatTime(totalDuration);

        if (wait) { // Wait ekranı gösteriliyorsa
          waitCounter += 0.1; // Wait ekranı kaç saniye gösterildiği hesaplanıyor
          if (waitCounter >= 5) { // 5 saniye bekledikten sonra
            wait = false; // Wait ekranı kapatılıyor
            waitCounter = 0;
            sendRawDataRequest();
            partCount = 1; // Part 1 geri sayımı başlatılıyor
            countdown = duration_part;
            if (partCount === 1) {
              startTimestamp = Date.now() / 1000; // Yeni tekrarın başlangıç zamanını al
              console.log("start: " + startTimestamp);
            } else {
              startTimestamp = endTimestamp; // Yeni tekrarın başlangıç zamanını bitiş zamanı olarak ayarla
              endTimestamp = Date.now() / 1000; // Bitiş zamanını güncelle
              console.log("start: " + startTimestamp);
            }            
          } else {
            popup.document.body.innerHTML = `
            <div style="font-size: 80px; font-weight: bold; text-align: center;">Wait...</div>
          `;
            return;
          }
        }
        popup.document.body.innerHTML = `
        <div style="font-size: 80px; font-weight: bold; text-align: center;">${formattedCountdown}</div>
        <div style="font-size: 30px; font-weight: bold; text-align: center;">Part ${partCount} / 1</div>
        <div style="font-size: 30px; font-weight: bold; text-align: center;">Repetition ${repetitionCount + 1} / ${repetitions}</div>
        <div style="text-align: center;">
          <label for="duration" style="font-size: 20px; font-weight: bold;">Total Duration:</label>
          <label id="duration" style="font-size: 20px;">${formattedTotalTime}</label>
        </div>
      `;
        const options = { timeZone: 'Europe/Istanbul' };
        const dateTime = new Date().toLocaleString('tr-TR', options);

        if (countdown <= 0) {
          const timestamp = Date.now() / 1000; // Start ve end timestamp'leri aynı anda hesapla
          startTimestamp = timestamp;
          endTimestamp = timestamp + duration_part;
          wait = true; // Wait ekranı gösterilecek
          const inputs = {
            company: company,
            expert: expert,
            username: username,
            current_part: partCount,
            total_number_part: 1,
            current_repeat: repetitionCount + 1,
            total_number_repeat: repetitions,
            duration_part: duration_part,
            total_duration: duration,
            startTimestamp_current_part: startTimestamp,
            endTimestamp_current_part: endTimestamp,
            date: dateTime, //new Date().toISOString().replace('T', ' ').substr(0, 19),
            //date: new Date().toISOString().substr(0, 10),
            config: config,
            posenumber: posenumber,
            description: description
          };

          logs.push(inputs);

          partCount++;

          if (partCount > 1) {
            repetitionCount++;
            this.rawdataService.stopRawData();
            if (repetitionCount >= repetitions) {
              this.postLogs(logs);
              clearInterval(countdownInterval);
              popup.alert("Finish.");
              popup.close();
            } else {
              partCount = 1;
              totalDuration = repetitions * duration_part;
            }
          }

          countdown = duration_part;
        }
        //localStorage.removeItem('config');
      }, delay * 100);
    }, 5000);
  }


  private formatTime(seconds: number): string {
    const remainingMilliseconds = Math.round((seconds - Math.floor(seconds)) * 100);
    const remainingSeconds = Math.floor(seconds) % 60;
    const remainingMinutes = Math.floor(seconds / 60);

    return `${remainingMinutes.toString().padStart(2, '0')}:${remainingSeconds.toString().padStart(2, '0')}.${remainingMilliseconds.toString().padStart(2, '0')}`;
  }
}
