import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KinectComponent } from './kinect.component';

describe('KinectComponent', () => {
  let component: KinectComponent;
  let fixture: ComponentFixture<KinectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KinectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KinectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
