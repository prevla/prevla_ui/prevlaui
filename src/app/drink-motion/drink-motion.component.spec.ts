import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinkMotionComponent } from './drink-motion.component';

describe('DrinkMotionComponent', () => {
  let component: DrinkMotionComponent;
  let fixture: ComponentFixture<DrinkMotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrinkMotionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DrinkMotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
