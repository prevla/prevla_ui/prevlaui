import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RadarComponent } from './radar/radar.component';
import { KinectComponent } from './kinect/kinect.component';
import { MediapipeComponent } from './mediapipe/mediapipe.component';
import { LeapMotionComponent } from './leap-motion/leap-motion.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AnasayfaComponent } from './anasayfa/anasayfa.component';
import { CommonModule } from '@angular/common';
import { SensorsConfigComponent } from './sensors-config/sensors-config.component';
import { MovementTrackerComponent } from './movement-tracker/movement-tracker.component';
import { KinectMotionComponent } from './kinect-motion/kinect-motion.component';
import { DrinkMotionComponent } from './drink-motion/drink-motion.component';
import { BoxMotionComponent } from './box-motion/box-motion.component';
import { ViewerComponent } from './viewer/viewer.component';
import { TimedgetMotionComponent } from './timedget-motion/timedget-motion.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RadarComponent,
    KinectComponent,
    MediapipeComponent,
    LeapMotionComponent,
    AnasayfaComponent,
    SensorsConfigComponent,
    MovementTrackerComponent,
    KinectMotionComponent,
    DrinkMotionComponent,
    BoxMotionComponent,
    ViewerComponent,
    TimedgetMotionComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
