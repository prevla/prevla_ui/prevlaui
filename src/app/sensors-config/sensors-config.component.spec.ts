import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorsConfigComponent } from './sensors-config.component';

describe('SensorsConfigComponent', () => {
  let component: SensorsConfigComponent;
  let fixture: ComponentFixture<SensorsConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SensorsConfigComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SensorsConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
