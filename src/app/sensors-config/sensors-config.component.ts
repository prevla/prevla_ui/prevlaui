import { Component } from '@angular/core';
import { MediaPipeData } from '../models/mediapipe.model';
import { MediapipeService } from '../services/mediapipe-service.service';
import { RadarServiceService } from '../services/radar-service.service';
import { KinectService } from '../services/kinect.service';
import { LeapService } from '../services/leap.service';

@Component({
  selector: 'app-sensors-config',
  templateUrl: './sensors-config.component.html',
  styleUrls: ['./sensors-config.component.css']
})
export class SensorsConfigComponent {
  data: MediaPipeData = new MediaPipeData('', '', '');
  isRunning: boolean = false;
  constructor(private mediapipeService: MediapipeService,
    private radarService: RadarServiceService ,
    private kinectService: KinectService,
    private leapService: LeapService) { }
    rawDataToggle: boolean = false;
  onSubmit() {
    window.alert("Mediapipe started");
    this.mediapipeService.postParameters(this.data).subscribe(response => {
      console.log(response);
    });
    this.data = new MediaPipeData('', '', ''); // inputların içeriğini temizle
  }
  ngOnInit() {
    this.sendcheckMediapipeRequest();
    this.sendcheckRadarRequest();
    const rawdataToggleValue = localStorage.getItem('rawDataToggle');
    
    if (rawdataToggleValue !== null) {
      this.rawDataToggle = JSON.parse(rawdataToggleValue);
    }
    
    // Sayfa yüklendiğinde "Status Activate" veya "Status Deactivate" yazısını ve renkleri güncelle (Mediapipe)
    const mediapipeStatusText = document.getElementById('status-text-mediapipe');
    const mediapipeStatusDisplay = document.getElementById('status-display-mediapipe');
    
    if (mediapipeStatusText && mediapipeStatusDisplay) {
      const mediapipeStoredToggle = localStorage.getItem('mediapipeToggle');
      
      if (mediapipeStoredToggle) {
        const isMediapipeToggleChecked = JSON.parse(mediapipeStoredToggle);
        
        if (isMediapipeToggleChecked) {
          mediapipeStatusText.textContent = 'Activate';
          mediapipeStatusDisplay.style.backgroundColor = 'green';
        } else {
          mediapipeStatusText.textContent = 'Deactivate';
          mediapipeStatusDisplay.style.backgroundColor = 'red';
        }
      }
    }
    
    // Sayfa yüklendiğinde "Status Activate" veya "Status Deactivate" yazısını ve renkleri güncelle (Radar)
    const radarStatusText = document.getElementById('status-text-radar');
    const radarStatusDisplay = document.getElementById('status-display');
    
    if (radarStatusText && radarStatusDisplay) {
      const radarStoredToggle = localStorage.getItem('radarToggle');
      
      if (radarStoredToggle) {
        const isRadarToggleChecked = JSON.parse(radarStoredToggle);
        
        if (isRadarToggleChecked) {
          radarStatusText.textContent = 'Activate';
          radarStatusDisplay.style.backgroundColor = 'green';
        } else {
          radarStatusText.textContent = 'Deactivate';
          radarStatusDisplay.style.backgroundColor = 'red';
        }
      }
    }
    const kinectStatusText = document.getElementById('status-text-kinect');
    const kinectStatusDisplay = document.getElementById('status-display-kinect');
  
    if (kinectStatusText && kinectStatusDisplay) {
      kinectStatusText.textContent = 'Deactivate';
      kinectStatusDisplay.style.backgroundColor = 'red';
    }
  
    // Sayfa yüklendiğinde "Status Activate" veya "Status Deactivate" yazısını ve renkleri güncelle (Leap Motion)
    const leapStatusText = document.getElementById('status-text-leap');
    const leapStatusDisplay = document.getElementById('status-display-leap');
  
    if (leapStatusText && leapStatusDisplay) {
      leapStatusText.textContent = 'Deactivate';
      leapStatusDisplay.style.backgroundColor = 'red';
    }
  }
  
  
  
  
  sendcheckRadarRequest() {
    const statusTextRadar = document.querySelector('#status-text-radar');
    const statusDisplayRadar = document.querySelector('#status-display-radar');
    const checkbox = document.querySelector('input[type="checkbox"]') as HTMLInputElement;
    const radarToggle = JSON.parse(localStorage.getItem('radarToggle') || 'false');

    this.radarService.sendCheckRequestWithoutData().subscribe(
      data => {
        if (data) {
          if (statusTextRadar) statusTextRadar.textContent = 'ON';
          if (statusDisplayRadar) statusDisplayRadar.classList.add('text-success');
          if (checkbox) checkbox.checked = true;
          if (radarToggle === true && checkbox) checkbox.checked = true;

        } else {
          if (statusTextRadar) statusTextRadar.textContent = 'OFF';
          if (statusDisplayRadar) statusDisplayRadar.classList.remove('text-success');
          if (checkbox) checkbox.checked = false;
          if (radarToggle === false && checkbox) checkbox.checked = false;
        }
      },
      error => console.log(error)
    );
  }
  radarToggle = JSON.parse(localStorage.getItem('radarToggle') || 'false');
  toggleSwitchRadar(event:any) {
    const isToggleChecked = event.target.checked;
    this.radarToggle = isToggleChecked;
    localStorage.setItem('radarToggle', JSON.stringify(isToggleChecked));
    if (isToggleChecked) {
      this.sendPostRequest();
    } else {
      this.sendClosePostRequest();
    }
}
// Toggle işlemi için fonksiyon
toggleSwitchRawData(event: any) {
  this.rawDataToggle = event.target.checked;
  localStorage.setItem('rawDataToggle', JSON.stringify(this.rawDataToggle));
  
  if (this.rawDataToggle) {
    // "Raw Data" açık olduğunda yapılacak işlemler
    console.log("Raw Data aktif");
    // İstenilen işlemleri burada gerçekleştirin
  } else {
    // "Raw Data" kapalı olduğunda yapılacak işlemler
    console.log("Raw Data kapalı");
    // İstenilen işlemleri burada gerçekleştirin
  }
}


sendPostRequest() {
  const config = JSON.parse(localStorage.getItem('config') || '{}');

  this.radarService.sendPostRequestWithData({ config: config }).subscribe(
    (data: any) => {
      if (data && data.status === 'ok') {
        const statusDisplay = document.getElementById('status-display');
        if (statusDisplay) {
          statusDisplay.style.backgroundColor = 'green';
          const statusText = document.getElementById('status-text-radar');
          if (statusText) {
            statusText.textContent = 'Activate';
          }
        }
        // Bilgilendirme mesajı göster
        alert('Operation has been successfully opened. Config: ' + JSON.stringify(config));

        // Config değerini sıfırla
        //localStorage.removeItem('config');
      }
    },
    (error: any) => console.log(error)
  );
}

  sendClosePostRequest() {
    const confirmed = confirm('Are you sure you want to close?');
    if (confirmed) {
      this.radarService.sendCloseGetRequest().subscribe(
        data => {
          if (data && data.status === 'ok') {
            const statusDisplay = document.getElementById('status-display');
            if (statusDisplay) {
              statusDisplay.style.backgroundColor = 'red';
              const statusText = document.getElementById('status-text-radar');
              if (statusText) {
                statusText.textContent = 'Deactivate';
              }
            }
          }
        },
        error => console.log(error)
      );
    } else {
      console.log('İşlem iptal edildi');
      localStorage.setItem('radarToggle', JSON.stringify(true));
      // Sayfayı yeniden yükle
      window.location.reload();
    }
  }


  sendcheckMediapipeRequest() {
    const statusTextMediapipe = document.querySelector('#status-text-mediapipe');
    const statusDisplayMediapipe = document.querySelector('#status-display-mediapipe');
    const checkbox = document.querySelector('input[type="checkbox"]') as HTMLInputElement;
    const mediapipeToggle = JSON.parse(localStorage.getItem('mediapipeToggle') || 'false');

    this.mediapipeService.postCheckMediapipe().subscribe(
      data => {
        if (data) {
          if (statusTextMediapipe) statusTextMediapipe.textContent = 'ON';
          if (statusDisplayMediapipe) statusDisplayMediapipe.classList.add('text-success');
          if (checkbox) checkbox.checked = true;
          if (mediapipeToggle === true && checkbox) checkbox.checked = true;

        } else {
          if (statusTextMediapipe) statusTextMediapipe.textContent = 'OFF';
          if (statusDisplayMediapipe) statusDisplayMediapipe.classList.remove('text-success');
          if (checkbox) checkbox.checked = false;
          if (mediapipeToggle === false && checkbox) checkbox.checked = false;
        }
      },
      error => console.log(error)
    );
  }

  mediapipeToggle = JSON.parse(localStorage.getItem('mediapipeToggle') || 'false');

  toggleSwitchMedia(event: any) {
    const isToggleChecked = event.target.checked;
    this.mediapipeToggle = isToggleChecked;
    localStorage.setItem('mediapipeToggle', JSON.stringify(isToggleChecked));
    if (isToggleChecked) {
      this.sendMediapipeRequest();
    } else {
      this.sendMediapipeCloseRequest();
    }
  }
  sendMediapipeRequest() {
    this.mediapipeService.postMediapipe().subscribe(
      data => {
        if (data && data.status === 'ok') {
          const statusDisplay = document.getElementById('status-display-mediapipe');
          if (statusDisplay) {
            statusDisplay.style.backgroundColor = 'green';
            const statusText = document.getElementById('status-text-mediapipe');
            if (statusText) {
              statusText.textContent = 'Activate';
            }
          }
          // Bilgilendirme mesajı göster
          alert('Operation has been successfully opened.');
        }
      },
      error => console.log(error)
    );
  }
  sendMediapipeCloseRequest() {
    const confirmed = confirm('Are you sure you want to close?');
    if (confirmed) {
      this.mediapipeService.postCloseMediapipe().subscribe(
        data => {
          if (data && data.status === 'ok') {
            const statusDisplay = document.getElementById('status-display-mediapipe');
            if (statusDisplay) {
              statusDisplay.style.backgroundColor = 'red';
              const statusText = document.getElementById('status-text-mediapipe');
              if (statusText) {
                statusText.textContent = 'Deactivate';
              }
            }
          }
        },
        error => console.log(error)
      );
    } else {
      // Kullanıcı iptal etti
    console.log('İşlem iptal edildi');
    localStorage.setItem('mediapipeToggle', JSON.stringify(true));
    // Sayfayı yeniden yükle
    window.location.reload();
    }
  }


  toggleSwitchKinect(event:any) {
    if (event.target.checked) {
        this.sendKinectRequest();
    } else {
        this.sendKinectCloseRequest();
    }
}
  sendKinectRequest() {
    this.kinectService.postKinect().subscribe(
      data => {
        if (data && data.status === 'ok') {
          const statusDisplay = document.getElementById('status-display-kinect');
          if (statusDisplay) {
            statusDisplay.style.backgroundColor = 'green';
            const statusText = document.getElementById('status-text-kinect');
            if (statusText) {
              statusText.textContent = 'Activate';
            }
          }
          // Bilgilendirme mesajı göster
          alert('Operation has been successfully opened.');
        }
      },
      error => console.log(error)
    );
  }
  sendKinectCloseRequest() {
    const confirmed = confirm('Are you sure you want to close?');
    if (confirmed) {
      this.kinectService.postCloseKinect().subscribe(
        data => {
          if (data && data.status === 'ok') {
            const statusDisplay = document.getElementById('status-display-kinect');
            if (statusDisplay) {
              statusDisplay.style.backgroundColor = 'red';
              const statusText = document.getElementById('status-text-kinect');
              if (statusText) {
                statusText.textContent = 'Deactivate';
              }
            }
          }
        },
        error => console.log(error)
      );
    } else {
      // Kullanıcı iptal etti
      console.log('İşlem iptal edildi');
    }
  }


  toggleSwitchLeap(event:any) {
    if (event.target.checked) {
        this.sendLeapRequest();
    } else {
        this.sendLeapCloseRequest();
    }
}
  sendLeapRequest() {
    this.leapService.postLeap().subscribe(
      data => {
        if (data && data.status === 'ok') {
          const statusDisplay = document.getElementById('status-display-leap');
          if (statusDisplay) {
            statusDisplay.style.backgroundColor = 'green';
            const statusText = document.getElementById('status-text-leap');
            if (statusText) {
              statusText.textContent = 'Activate';
            }
          }
          // Bilgilendirme mesajı göster
          alert('Operation has been successfully opened.');
        }
      },
      error => console.log(error)
    );
  }
  sendLeapCloseRequest() {
    const confirmed = confirm('Are you sure you want to close?');
    if (confirmed) {
      this.leapService.postCloseLeap().subscribe(
        data => {
          if (data && data.status === 'ok') {
            const statusDisplay = document.getElementById('status-display-leap');
            if (statusDisplay) {
              statusDisplay.style.backgroundColor = 'red';
              const statusText = document.getElementById('status-text-leap');
              if (statusText) {
                statusText.textContent = 'Deactivate';
              }
            }
          }
        },
        error => console.log(error)
      );
    } else {
      // Kullanıcı iptal etti
      console.log('İşlem iptal edildi');
    }
  }
}
