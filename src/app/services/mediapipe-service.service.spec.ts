import { TestBed } from '@angular/core/testing';

import { MediapipeServiceService } from './mediapipe-service.service';

describe('MediapipeServiceService', () => {
  let service: MediapipeServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MediapipeServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
