import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/enviroments/enviroments';

@Injectable({
  providedIn: 'root'
})
export class KinectService {

  private url = `${environment.apiUrl}/sensor/open/kinect`;
  private closeUrl = `${environment.apiUrl}/close`;
  constructor(private http: HttpClient) { }

  postKinect(): Observable<any> {
    return this.http.post<any>(this.url, {});
  }
  postCloseKinect(): Observable<any> {
    return this.http.post<any>(this.closeUrl,{});
  }
}
