import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/enviroments/enviroments';

@Injectable({
  providedIn: 'root'
})
export class RawdataService {
  private url = `${environment.apiUrl}/raw_start`;
  private stopUrl = `${environment.apiUrl}/raw_stop`;
  constructor(private http: HttpClient) { }

  toggleSwitchRawData(isToggleChecked: boolean, total_time: number, rawdata: any): void {
    if (!isToggleChecked) {
      throw new Error("rawDataToggle değeri false!");
    }
  
    const postData = {
      rawDataToggle: isToggleChecked,
      total_time: total_time,
      repeat: rawdata.repeat // Include the repetitions value in the payload
    };
  
    this.http.post(this.url, postData)
      .subscribe(
        response => {
          console.log('Post request successful', response);
          // Perform further actions here
        },
        error => {
          console.error('Post request error', error);
          // Handle error cases here
        }
      );
  }
  stopRawData(): void {
    const rawDataToggleValue = localStorage.getItem('rawDataToggle');
    if (rawDataToggleValue === 'true') {
      this.http.post(this.stopUrl, {})
        .subscribe(
          response => {
            console.log('Stop request successful', response);
            // Perform further actions here
          },
          error => {
            console.error('Stop request error', error);
            // Handle error cases here
          }
        );
    }
  }
  
  
}

