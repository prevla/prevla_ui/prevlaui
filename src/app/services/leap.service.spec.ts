import { TestBed } from '@angular/core/testing';

import { LeapService } from './leap.service';

describe('LeapService', () => {
  let service: LeapService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LeapService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
