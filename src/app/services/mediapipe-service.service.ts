import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MediaPipeData } from '../models/mediapipe.model';
import { Observable } from 'rxjs';
import { environment } from 'src/enviroments/enviroments';

@Injectable({
  providedIn: 'root'
})
export class MediapipeService {
  private url = `${environment.apiUrl}/sensor/open/mediapipe`;
  private closeUrl = `${environment.apiUrl}/sensor/close/mediapipe`;
  private checkUrl = `${environment.apiUrl}/sensor/check/mediapipe`
  constructor(private http: HttpClient) { }

  postParameters(data: MediaPipeData): Observable<any> {
    return this.http.post<any>(this.url, data);
  }

  postMediapipe(): Observable<any> {
    return this.http.post<any>(this.url, {});
  }
  postCloseMediapipe(): Observable<any> {
    return this.http.post<any>(this.closeUrl,{});
  }
  postCheckMediapipe(): Observable<any> {
    return this.http.post<any>(this.checkUrl,{});
  }

}
