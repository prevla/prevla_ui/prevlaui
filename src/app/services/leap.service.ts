import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/enviroments/enviroments';

@Injectable({
  providedIn: 'root'
})
export class LeapService {

  private url = `${environment.apiUrl}/sensor/open/leap`;
  private closeUrl = `${environment.apiUrl}/close`;
  constructor(private http: HttpClient) { }

  postLeap(): Observable<any> {
    return this.http.post<any>(this.url, {});
  }
  postCloseLeap(): Observable<any> {
    return this.http.post<any>(this.closeUrl,{});
  }
}
