import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/enviroments/enviroments';

@Injectable({
  providedIn: 'root'
})
export class ViewerService {
  private url = `${environment.apiUrl}/expList`;
  private figureUrl = `${environment.apiUrl}/figure`;
  constructor(private http: HttpClient) { }
  getList() {
    return this.http.post<any[]>(this.url, {});
  }
  postFigure(exp: number) {
    return this.http.post(this.figureUrl, { exp });
  }
}
