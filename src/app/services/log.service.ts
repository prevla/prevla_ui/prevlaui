import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/enviroments/enviroments';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  private url = `${environment.apiUrl}/save/log`;

  constructor(private http: HttpClient) { }

  postLog(logs: any[]) {
    return this.http.post(this.url, logs);
  }
}
