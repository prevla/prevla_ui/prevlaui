import { TestBed } from '@angular/core/testing';

import { KinectService } from './kinect.service';

describe('KinectService', () => {
  let service: KinectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KinectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
