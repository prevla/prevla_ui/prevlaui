import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/enviroments/enviroments';

@Injectable({
  providedIn: 'root'
})
export class RadarServiceService {
  private url = `${environment.apiUrl}/sensor/open/radar`;
  private closeUrl = `${environment.apiUrl}/sensor/close/radar`;
  private checkUrl = `${environment.apiUrl}/sensor/check/radar`;
  constructor(private http: HttpClient) { }


  sendPostRequestWithoutData(): Observable<any> {
    return this.http.post<any>(this.url, {});
  }
  sendPostRequestWithData(data: any): Observable<any> {
    return this.http.post<any>(this.url, data);
  }

  sendCloseGetRequest(): Observable<any> {
    return this.http.post<any>(this.closeUrl,{});
  }

  sendCheckRequestWithoutData(): Observable<any> {
    return this.http.post<any>(this.checkUrl, {});
  }
}

