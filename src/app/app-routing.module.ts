import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnasayfaComponent } from './anasayfa/anasayfa.component';
import { BoxMotionComponent } from './box-motion/box-motion.component';
import { DrinkMotionComponent } from './drink-motion/drink-motion.component';
import { KinectMotionComponent } from './kinect-motion/kinect-motion.component';
import { KinectComponent } from './kinect/kinect.component';
import { LeapMotionComponent } from './leap-motion/leap-motion.component';
import { MediapipeComponent } from './mediapipe/mediapipe.component';
import { MovementTrackerComponent } from './movement-tracker/movement-tracker.component';
import { RadarComponent } from './radar/radar.component';
import { SensorsConfigComponent } from './sensors-config/sensors-config.component';
import { ViewerComponent } from './viewer/viewer.component';
import { TimedgetMotionComponent } from './timedget-motion/timedget-motion.component';

const routes: Routes = [
  { path: '', component: AnasayfaComponent },
  { path: 'sensors-config', component: SensorsConfigComponent },
  { path: 'radar', component: RadarComponent },
  { path: 'kinect', component: KinectComponent },
  { path: 'mediapipe', component: MediapipeComponent },
  { path: 'leap-motion', component: LeapMotionComponent },
  { path: 'anasayfa', component: AnasayfaComponent },
  {path: 'movement', component: MovementTrackerComponent},
  {path: 'kinect-motion', component: KinectMotionComponent},
  {path: 'drink-motion', component: DrinkMotionComponent},
  {path: 'box-motion', component: BoxMotionComponent},
  {path: 'timedget-motion', component: TimedgetMotionComponent},
  {path: 'viewer', component: ViewerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
