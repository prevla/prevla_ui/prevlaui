export class MediaPipeData {
  constructor(
    public cam_port: string,
    public name: string,
    public exp_no: string
  ) { }
}
