import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KinectMotionComponent } from './kinect-motion.component';

describe('KinectMotionComponent', () => {
  let component: KinectMotionComponent;
  let fixture: ComponentFixture<KinectMotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KinectMotionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KinectMotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
