import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimedgetMotionComponent } from './timedget-motion.component';

describe('TimedgetMotionComponent', () => {
  let component: TimedgetMotionComponent;
  let fixture: ComponentFixture<TimedgetMotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimedgetMotionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TimedgetMotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
