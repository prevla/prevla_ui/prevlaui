import { Component } from '@angular/core';
import { ViewerService } from '../services/viewer.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.css']
})
export class ViewerComponent {
  itemList: any[] = [];
  datePipe: DatePipe = new DatePipe('en-US'); // DatePipe eklenmiş
  searchValue: string = '';
  startDate: string = '';
  endDate: string = '';

  constructor(private viewerService: ViewerService) {}

  ngOnInit(): void {
    this.fetchItemList();
  }
  

  fetchItemList(): void {
    this.viewerService.getList().subscribe(
      data => {
        this.itemList = Object.values(data);
      },
      error => {
        console.error('Error fetching item list', error);
      }
    );
  }
  
  selectItem(item: any): void {
    this.viewerService.postFigure(item.deney_no).subscribe(
      response => {
        // Seçilen öğeyle ilgili istek başarılı şekilde gönderildi
        console.log("Figure request successful:", response);
      },
      error => {
        console.error('Error sending figure request', error);
      }
    );
  }
  formatTimestamp(timestamp: number | null): string {
    if (timestamp) {
      const dateObj: Date = new Date(timestamp);
      const formattedDate: string = this.datePipe.transform(dateObj, 'dd.MM.yyyy HH:mm:ss') || '';
      return formattedDate;
    }
    return '';
  }
  getPoseNumberLabel(poseNumber: number): string {
    if (poseNumber === 1) {
      return 'Sol Kol';
    } else if (poseNumber === 2) {
      return 'Sağ Kol';
    } else {
      return '';
    }
  }
  filterItems(): any[] {
    let filteredItems = this.itemList;
  
    if (this.searchValue) {
      const searchNumber = parseInt(this.searchValue);
      const searchPatient = this.searchValue;
      filteredItems = filteredItems.filter(item =>
        item.deney_no.toString().includes(searchNumber.toString()) ||
        item.patient_name.includes(searchPatient)
      );
    }
    
    if (this.startDate && this.endDate) {
      const startDate = new Date(this.startDate);
      const endDate = new Date(this.endDate);
      endDate.setDate(endDate.getDate() + 1); // endDate'yi bir gün sonraya ayarla
      filteredItems = filteredItems.filter(item => {
        const itemDate = new Date(item.date);
        return (
          itemDate >= startDate &&
          itemDate < endDate
        );
      });
    }
  
    return filteredItems;
  }
  
}
