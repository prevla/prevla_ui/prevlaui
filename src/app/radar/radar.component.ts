import { Component } from '@angular/core';
import { RadarServiceService } from '../services/radar-service.service';

declare global {
  interface Window {
    saveConfig(): void;
  }
}
@Component({
  selector: 'app-radar',
  templateUrl: './radar.component.html',
  styleUrls: ['./radar.component.css']
})
export class RadarComponent {
//   constructor(private radarService: RadarServiceService) { }
//   sendPostRequest() {
//     this.radarService.sendPostRequestWithoutData().subscribe(
//       data => console.log(data),
//       error => console.log(error)
//     );
// }

// sendClosePostRequest() {
//   this.radarService.sendCloseGetRequest().subscribe(
//     data => console.log(data),
//     error => console.log(error)
//   );
// }

disableOtherCheckboxes(selectedCheckbox: EventTarget | null) {
  const checkboxes = document.getElementsByName('config') as NodeListOf<HTMLInputElement>;

  const alternativeConfigInput = document.getElementById('alternative-config') as HTMLInputElement;

  if (selectedCheckbox) {
    alternativeConfigInput.disabled = true;
    checkboxes.forEach((checkbox) => {
      if (checkbox !== selectedCheckbox) {
        checkbox.disabled = (selectedCheckbox as HTMLInputElement).checked;
      }
    });
  } else {
    alternativeConfigInput.disabled = false;
    checkboxes.forEach((checkbox) => {
      checkbox.disabled = true;
    });
  }
}

resetConfig() {
  const checkboxes = document.getElementsByName('config') as NodeListOf<HTMLInputElement>;
  const alternativeConfigInput = document.getElementById('alternative-config') as HTMLInputElement;

  // checkboxları ve alternative config inputunu sıfırla
  checkboxes.forEach((checkbox) => {
    checkbox.checked = false;
    checkbox.disabled = false;
  });
  alternativeConfigInput.value = '';
  alternativeConfigInput.disabled = false;

}

saveConfig() {
  const checkboxes = document.querySelectorAll('input[type="checkbox"]');
  let configValue: number | undefined;
  checkboxes.forEach((checkbox) => {
    if ((checkbox as HTMLInputElement).checked) {
      configValue = parseInt((checkbox as HTMLInputElement).value.substring(6)); // checkbox value is "configX"
    }
  });
  const alternativeConfigInput = document.querySelector<HTMLInputElement>('#alternative-config');
  if (alternativeConfigInput && alternativeConfigInput.value !== '') {
    configValue = parseInt(alternativeConfigInput.value);
  }
  if (configValue !== undefined) {
    localStorage.setItem('config', configValue.toString());
    this.resetConfig(); // save butonuna tıklanıldığında değerleri sıfırla
    alert('Success: Config has been saved.');
    window.location.href = '/sensors-config'; // redirect to sensors-config page
  }
}

constructor(private radarService: RadarServiceService) {
  window.saveConfig = this.saveConfig.bind(this);
}
}
